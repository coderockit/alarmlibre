import 'dart:io';

import 'package:alarm/alarm.dart';
import 'package:alarm/model/alarm_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ExampleAlarmEditScreen extends StatefulWidget {
  const ExampleAlarmEditScreen({super.key, this.alarmSettings});

  final AlarmSettings? alarmSettings;

  @override
  State<ExampleAlarmEditScreen> createState() => _ExampleAlarmEditScreenState();
}

class _ExampleAlarmEditScreenState extends State<ExampleAlarmEditScreen> {
  bool loading = false;

  late String alarmName;
  late bool creating;
  late DateTime selectedDateTime;
  late bool loopAudio;
  late int repeatSoundLoops;
  late int snoozeDuration;
  late int autoRepeatDuration;
  late bool vibrate;
  late double? volume;
  late String assetAudio;

  @override
  void initState() {
    super.initState();
    creating = widget.alarmSettings == null;

    if (creating) {
      alarmName = 'alarm 1';
      selectedDateTime = DateTime.now().add(const Duration(minutes: 2));
      selectedDateTime = selectedDateTime.copyWith(second: 0, millisecond: 0);
      loopAudio = true;
      repeatSoundLoops = -1;
      vibrate = true;
      volume = null;
      assetAudio = 'assets/sounds/marimba.mp3';
      snoozeDuration = 5;
      autoRepeatDuration = 0;
      print("Creating the alarm settings brand new... $assetAudio");
    } else {
      alarmName = widget.alarmSettings!.alarmName;
      selectedDateTime = widget.alarmSettings!.dateTime;
      loopAudio = widget.alarmSettings!.loopAudio;
      repeatSoundLoops = widget.alarmSettings!.repeatSoundLoops;
      vibrate = widget.alarmSettings!.vibrate;
      volume = widget.alarmSettings!.volume;
      assetAudio = widget.alarmSettings!.assetAudioPath;
      snoozeDuration = widget.alarmSettings!.snoozeDuration;
      autoRepeatDuration = widget.alarmSettings!.autoRepeatDuration;
      print("Using existing alarm settings... $alarmName");
    }
  }

  String getDay() {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final difference = selectedDateTime.difference(today).inDays;

    switch (difference) {
      case 0:
        return 'Today';
      case 1:
        return 'Tomorrow';
      case 2:
        return 'After tomorrow';
      default:
        return 'In $difference days';
    }
  }

  Future<void> pickTime() async {
    final res = await showTimePicker(
      initialTime: TimeOfDay.fromDateTime(selectedDateTime),
      context: context,
    );

    if (res != null) {
      setState(() {
        final now = DateTime.now();
        selectedDateTime = now.copyWith(
          hour: res.hour,
          minute: res.minute,
          second: 0,
          millisecond: 0,
          microsecond: 0,
        );
        if (selectedDateTime.isBefore(now)) {
          selectedDateTime = selectedDateTime.add(const Duration(days: 1));
        }
      });
    }
  }

  AlarmSettings buildAlarmSettings() {
    final id = creating
        ? DateTime.now().millisecondsSinceEpoch % 10000 + 1
        : widget.alarmSettings!.id;

    print("Building alarm settings... $assetAudio");
    final alarmSettings = AlarmSettings(
      id: id,
      alarmName: alarmName,
      dateTime: selectedDateTime,
      loopAudio: loopAudio,
      repeatSoundLoops: repeatSoundLoops,
      vibrate: vibrate,
      volume: volume,
      assetAudioPath: assetAudio,
      notificationTitle: alarmName,
      notificationBody: 'Your alarm $alarmName is ringing',
      enableNotificationOnKill: Platform.isIOS,
      snoozeDuration: snoozeDuration,
      autoRepeatDuration: autoRepeatDuration,
    );
    return alarmSettings;
  }

  void saveAlarm() {
    if (loading) return;
    setState(() => loading = true);
    Alarm.set(alarmSettings: buildAlarmSettings()).then((res) {
      if (res) Navigator.pop(context, true);
      setState(() => loading = false);
    });
  }

  void snoozeAlarm() {
    Alarm.stop(widget.alarmSettings!).then((res) {
      if (res) Navigator.pop(context, true);
    });
    final now = DateTime.now();
    print("Snoozing alarm settings... ${widget.alarmSettings!.assetAudioPath}");
    Alarm.set(
      alarmSettings: widget.alarmSettings!.copyWith(
        id: now.millisecondsSinceEpoch % 10000 + 1,
        dateTime: DateTime(
          now.year,
          now.month,
          now.day,
          now.hour,
          now.minute,
        ).add(Duration(minutes: widget.alarmSettings!.snoozeDuration)),
      ),
    );
  }

  void deleteAlarm() {
    Alarm.stop(widget.alarmSettings!).then((res) {
      if (res) Navigator.pop(context, true);
      if (widget.alarmSettings!.autoRepeatDuration > 0) {
        final now = DateTime.now();
        print(
            "Auto-repeating alarm with settings... ${widget.alarmSettings!.assetAudioPath}");
        Alarm.set(
          alarmSettings: widget.alarmSettings!.copyWith(
            id: now.millisecondsSinceEpoch % 10000 + 1,
            dateTime: DateTime(
              now.year,
              now.month,
              now.day,
              now.hour,
              now.minute,
            ).add(Duration(minutes: widget.alarmSettings!.autoRepeatDuration)),
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
      child: ListView(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(color: Colors.blueAccent),
                ),
              ),
              if (widget.alarmSettings != null)
                Text(widget.alarmSettings!.alarmName),
              TextButton(
                onPressed: saveAlarm,
                child: loading
                    ? const CircularProgressIndicator()
                    : Text(
                        'Save',
                        style: Theme.of(context)
                            .textTheme
                            .titleLarge!
                            .copyWith(color: Colors.blueAccent),
                      ),
              ),
            ],
          ),
          Text(
            getDay(),
            style: Theme.of(context)
                .textTheme
                .titleMedium!
                .copyWith(color: Colors.blueAccent.withOpacity(0.8)),
          ),
          RawMaterialButton(
            onPressed: pickTime,
            fillColor: Colors.grey[200],
            child: Container(
              margin: const EdgeInsets.all(20),
              child: Text(
                TimeOfDay.fromDateTime(selectedDateTime).format(context),
                style: Theme.of(context)
                    .textTheme
                    .displayMedium!
                    .copyWith(color: Colors.blueAccent),
              ),
            ),
          ),
          SizedBox(
            child: TextFormField(
              initialValue: alarmName,
              onChanged: (value) {
                setState(() => alarmName = value);
              },
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Alarm Name',
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Loop alarm audio',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              Switch(
                value: loopAudio,
                onChanged: (value) => setState(() => loopAudio = value),
              ),
            ],
          ),
          SizedBox(
            height: loopAudio ? null : 0,
            width: loopAudio ? null : 0,
            child: loopAudio
                ? TextFormField(
                    initialValue: repeatSoundLoops.toString(),
                    onChanged: (value) {
                      setState(() => repeatSoundLoops =
                          int.tryParse(value) ?? repeatSoundLoops);
                    },
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: 'Loop audio X times',
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'^-?\d{0,12}')),
                    ], // Only numbers can be entered
                  )
                : null,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Vibrate',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              Switch(
                value: vibrate,
                onChanged: (value) => setState(() => vibrate = value),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Sound',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              DropdownButton(
                value: assetAudio,
                items: const [
                  DropdownMenuItem<String>(
                    value: 'assets/sounds/marimba.mp3',
                    child: Text('Marimba'),
                  ),
                  DropdownMenuItem<String>(
                    value: 'assets/sounds/nokia.mp3',
                    child: Text('Nokia'),
                  ),
                  DropdownMenuItem<String>(
                    value: 'assets/sounds/mozart.mp3',
                    child: Text('Mozart'),
                  ),
                  DropdownMenuItem<String>(
                    value: 'assets/sounds/star_wars.mp3',
                    child: Text('Star Wars'),
                  ),
                  DropdownMenuItem<String>(
                    value: 'assets/sounds/one_piece.mp3',
                    child: Text('One Piece'),
                  ),
                ],
                onChanged: (value) => setState(() => assetAudio = value!),
              ),
            ],
          ),
          SizedBox(
            child: TextFormField(
              initialValue: snoozeDuration.toString(),
              onChanged: (value) {
                setState(() =>
                    snoozeDuration = int.tryParse(value) ?? snoozeDuration);
              },
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Snooze Duration in minutes',
              ),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'^\d{0,12}')),
              ], // Only numbers can be entered
            ),
          ),
          SizedBox(
            child: TextFormField(
              initialValue: autoRepeatDuration.toString(),
              onChanged: (value) {
                setState(() => autoRepeatDuration =
                    int.tryParse(value) ?? autoRepeatDuration);
              },
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Auto-Repeat After X minutes',
              ),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'^\d{0,12}')),
              ], // Only numbers can be entered
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Custom volume',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              Switch(
                value: volume != null,
                onChanged: (value) =>
                    setState(() => volume = value ? 0.5 : null),
              ),
            ],
          ),
          SizedBox(
            height: volume != null ? 30 : null,
            child: volume != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        volume! > 0.7
                            ? Icons.volume_up_rounded
                            : volume! > 0.1
                                ? Icons.volume_down_rounded
                                : Icons.volume_mute_rounded,
                      ),
                      Expanded(
                        child: Slider(
                          value: volume!,
                          onChanged: (value) {
                            setState(() => volume = value);
                          },
                        ),
                      ),
                    ],
                  )
                : null,
          ),
          if (!creating)
            Row(children: [
              TextButton(
                onPressed: deleteAlarm,
                child: Text(
                  'Delete Alarm',
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(color: Colors.red),
                ),
              ),
              TextButton(
                onPressed: snoozeAlarm,
                child: Text(
                  'Snooze Alarm',
                  style: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(color: Colors.deepPurple),
                ),
              )
            ]),
          const SizedBox(),
        ],
      ),
    );
  }
}
