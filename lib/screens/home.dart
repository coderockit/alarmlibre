import 'dart:async';

import 'package:alarm/alarm.dart';
import 'package:alarm/model/alarm_event.dart';
import 'package:alarm/model/alarm_settings.dart';
import 'package:alarmlibre/screens/edit_alarm.dart';
import 'package:alarmlibre/screens/ring.dart';
import 'package:alarmlibre/screens/shortcut_button.dart';
import 'package:alarmlibre/widgets/tile.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class ExampleAlarmHomeScreen extends StatefulWidget {
  const ExampleAlarmHomeScreen({super.key});

  @override
  State<ExampleAlarmHomeScreen> createState() => _ExampleAlarmHomeScreenState();
}

class _ExampleAlarmHomeScreenState extends State<ExampleAlarmHomeScreen> {
  bool popOffRingScreen = false;

  late List<AlarmSettings> alarms;

  static StreamSubscription<AlarmEvent>? subscription;

  @override
  void initState() {
    super.initState();
    if (Alarm.android) {
      checkAndroidNotificationPermission();
      checkAndroidScheduleExactAlarmPermission();
    }
    loadAlarms();
    print("Initializing subscription to stream...");
    subscription ??= Alarm.ringStream.stream.listen(navigateToRingScreen);
  }

  void loadAlarms() {
    setState(() {
      alarms = Alarm.getAlarms();
      alarms.sort((a, b) => a.dateTime.isBefore(b.dateTime) ? 0 : 1);
    });
  }

  Future<void> navigateToRingScreen(AlarmEvent alarmEvent) async {
    if (alarmEvent.eventType == AlarmEventType.RingingStopped) {
      print(
          "Alarm is being stopped: ${alarmEvent.alarm.id} and ${alarmEvent.alarm.dateTime} and ${alarmEvent.alarm.autoRepeatDuration} and ${alarmEvent.alarm.snoozeDuration}");

      if (alarmEvent.eventSource == 'iosAlarmStoppedCallback') {
        Alarm.stop(alarmEvent.alarm);

        if (alarmEvent.alarm.autoRepeatDuration > 0) {
          final now = DateTime.now();
          print(
              "Auto-repeating alarm with settings... ${alarmEvent.alarm.assetAudioPath}");
          Alarm.set(
            alarmSettings: alarmEvent.alarm.copyWith(
              id: now.millisecondsSinceEpoch % 10000 + 1,
              dateTime: DateTime(
                now.year,
                now.month,
                now.day,
                now.hour,
                now.minute,
              ).add(Duration(minutes: alarmEvent.alarm.autoRepeatDuration)),
            ),
          );
        }
      }

      if (popOffRingScreen /*&&
          alarmEvent.alarm.loopAudio &&
          (alarmEvent.alarm.repeatSoundLoops <= 0 ||
              alarmEvent.alarm.repeatSoundLoops > 5)*/
          ) {
        setState(() => popOffRingScreen = false);
        print("popping off ring screen in HOME screen");
        Navigator.pop(context);
      }
    } else {
      // if (alarmEvent.alarm.loopAudio &&
      //     (alarmEvent.alarm.repeatSoundLoops <= 0 ||
      //         alarmEvent.alarm.repeatSoundLoops > 5)) {
      setState(() => popOffRingScreen = true);
      await Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (context) => PopScope(
              onPopInvoked: (didPop) => setState(() {
                    popOffRingScreen = !didPop;
                  }),
              child: ExampleAlarmRingScreen(alarmSettings: alarmEvent.alarm)),
        ),
      );
      // }
      loadAlarms();
    }
  }

  Future<void> navigateToAlarmScreen(AlarmSettings? settings) async {
    final res = await showModalBottomSheet<bool?>(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      enableDrag: true,
      // shape: RoundedRectangleBorder(
      //   borderRadius: BorderRadius.circular(10),
      // ),
      builder: (context) {
        return FractionallySizedBox(
            heightFactor: 0.75,
            child: Scaffold(
              body: ExampleAlarmEditScreen(alarmSettings: settings),
            ));
      },
    );

    if (res != null && res == true) loadAlarms();
  }

  Future<void> checkAndroidNotificationPermission() async {
    final status = await Permission.notification.status;
    if (status.isDenied) {
      alarmPrint('Requesting notification permission...');
      final res = await Permission.notification.request();
      alarmPrint(
        'Notification permission ${res.isGranted ? '' : 'not '}granted',
      );
    }
  }

  Future<void> checkAndroidExternalStoragePermission() async {
    final status = await Permission.storage.status;
    if (status.isDenied) {
      alarmPrint('Requesting external storage permission...');
      final res = await Permission.storage.request();
      alarmPrint(
        'External storage permission ${res.isGranted ? '' : 'not'} granted',
      );
    }
  }

  Future<void> checkAndroidScheduleExactAlarmPermission() async {
    final status = await Permission.scheduleExactAlarm.status;
    alarmPrint('Schedule exact alarm permission: $status.');
    if (status.isDenied) {
      alarmPrint('Requesting schedule exact alarm permission...');
      final res = await Permission.scheduleExactAlarm.request();
      alarmPrint(
        'Schedule exact alarm permission ${res.isGranted ? '' : 'not'} granted',
      );
    }
  }

  @override
  void dispose() {
    print("Cancelling subscription to stream...");
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('AlarmLibre')),
      body: SafeArea(
        child: alarms.isNotEmpty
            ? ListView.separated(
                itemCount: alarms.length,
                separatorBuilder: (context, index) => const Divider(height: 1),
                itemBuilder: (context, index) {
                  final timeOfDay = TimeOfDay(
                    hour: alarms[index].dateTime.hour,
                    minute: alarms[index].dateTime.minute,
                  ).format(context);
                  return ExampleAlarmTile(
                    key: Key(alarms[index].id.toString()),
                    title: "${alarms[index].alarmName} at $timeOfDay",
                    onPressed: () => navigateToAlarmScreen(alarms[index]),
                    onDismissed: () {
                      Alarm.stop(alarms[index]).then((_) => loadAlarms());
                    },
                  );
                },
              )
            : Center(
                child: Text(
                  'No alarms set',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ExampleAlarmHomeShortcutButton(refreshAlarms: loadAlarms),
            FloatingActionButton(
              onPressed: () => navigateToAlarmScreen(null),
              child: const Icon(Icons.alarm_add_rounded, size: 33),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
