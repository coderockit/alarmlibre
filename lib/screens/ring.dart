import 'package:alarm/alarm.dart';
import 'package:alarm/model/alarm_settings.dart';
import 'package:flutter/material.dart';

class ExampleAlarmRingScreen extends StatelessWidget {
  const ExampleAlarmRingScreen({required this.alarmSettings, super.key});

  final AlarmSettings alarmSettings;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              'Your alarm ${alarmSettings.alarmName} is ringing...',
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const Text('🔔', style: TextStyle(fontSize: 50)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                RawMaterialButton(
                  onPressed: () {
                    Alarm.stop(alarmSettings);
                    final now = DateTime.now();
                    print(
                        "Snoozing alarm settings... ${alarmSettings.assetAudioPath}");
                    Alarm.set(
                      alarmSettings: alarmSettings.copyWith(
                        id: now.millisecondsSinceEpoch % 10000 + 1,
                        dateTime: DateTime(
                          now.year,
                          now.month,
                          now.day,
                          now.hour,
                          now.minute,
                        ).add(Duration(minutes: alarmSettings.snoozeDuration)),
                      ),
                    ); //.then((_) => Navigator.pop(context));
                    Navigator.pop(context);
                    print("popping off ring screen in RING screen");
                  },
                  child: Text(
                    'Snooze',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                RawMaterialButton(
                  onPressed: () {
                    Alarm.stop(alarmSettings);
                    //.then((_) => Navigator.pop(context));
                    if (alarmSettings.autoRepeatDuration > 0) {
                      final now = DateTime.now();
                      print(
                          "Auto-repeating alarm with settings... ${alarmSettings.assetAudioPath}");
                      Alarm.set(
                        alarmSettings: alarmSettings.copyWith(
                          id: now.millisecondsSinceEpoch % 10000 + 1,
                          dateTime: DateTime(
                            now.year,
                            now.month,
                            now.day,
                            now.hour,
                            now.minute,
                          ).add(Duration(
                              minutes: alarmSettings.autoRepeatDuration)),
                        ),
                      );
                    }
                    Navigator.pop(context);
                    print("popping off ring screen in RING screen");
                  },
                  child: Text(
                    'Stop',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                RawMaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                    print("popping off ring screen in RING screen");
                  },
                  child: Text(
                    '< GoBack',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
